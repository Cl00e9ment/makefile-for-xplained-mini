# Instructions to use this repo with VS Code

| OS               | Compatibility            |
| ---------------- | ------------------------ |
| Linux            | 🟢 tested                |
| macOS            | 🟡 may work (not tested) |
| Windows with WSL | 🟡 may work (not tested) |
| Windows          | 🔴 doesn't work          |

## Install The Required Extensions

1. hit `Ctrl` + `Shift` + `P`
2. select `Extensions: Show Recommended Extensions`
3. install them
4. if you have installed the `PlatformIO IDE` extension, it is recommended to disable it in this repository to avoid conflicts

## Building and uploading

1. plug the board
2. hit `Ctrl` + `Shift` + `B`
3. profit

## Debugging

1. plug the board
2. hit `F5`
3. wait for the message asking you to reconnect the board, and then reconnect the board
4. profit

## Using the serial console

1. plug the board
2. hit `Ctrl` + `Shift` + `P`
3. select `Tasks: Run Task`
4. select `XPlained Mini: Serial Console`
5. profit
6. to exit the console, hit `Ctrl` + `A` and then `Ctrl` + `X`
