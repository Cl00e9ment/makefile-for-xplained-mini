# Instructions to use this repo with Make

## Building and uploading

1. run `make`
2. plug the board
3. run `make upload_release`
4. profit

## Debugging

1. run `make build_debug`
2. plug the board
3. run `make upload_debug`
4. enable the DWEN (debug wire enabled) fuse bit by running `make enable_debug`
5. reconnect the board
6. start the debug server by running `make run_debug_server`  
   *If it fails it's probably because you programmed the device with SPI and did not not reconnect the board before using the debug wire interface, so don't forget `step 3`.*
7. run gdb with `make run_gdb`
8. profit

## Using the serial console

1. plug the board
2. run `make serial_console`
3. profit
4. to exit the console, hit `Ctrl` + `A` and then `Ctrl` + `X`
