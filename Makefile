CC = avr-gcc

CFLAGS  = -mmcu=atmega328p -DF_CPU=16000000L
CFLAGS += -Wall -Wextra
CFLAGS += -fno-fat-lto-objects -ffunction-sections -fdata-sections -flto
CFLAGS += -Iinclude 

DEBUG_FLAGS   = -g3
RELEASE_FLAGS = -Os

ADFLAGS = -p m328p -c xplainedmini

SOURCES         = $(wildcard src/*.c)
DEBUG_OBJECTS   = $(SOURCES:src/%.c=build/debug/%.o)
RELEASE_OBJECTS = $(SOURCES:src/%.c=build/release/%.o)

.PHONY: all build_debug build_release upload_debug upload_release clean enable_debug disable_debug run_debug_server run_gdb

all: build_release

build_debug: build/debug/firmware.elf

build_release: build/release/firmware.elf

upload_debug:
	avrdude $(ADFLAGS) -U flash:w:build/debug/firmware.elf

upload_release:
	avrdude $(ADFLAGS) -U flash:w:build/release/firmware.elf

serial_console:
	picocom /dev/ttyACM0

enable_debug:
	avrdude $(ADFLAGS) -U hfuse:w:0x99:m

disable_debug:
	avrdude $(ADFLAGS) -U hfuse:w:0xd9:m

run_debug_server:
	cd debug && bloom

run_gdb:
	avr-gdb -ex 'target remote :1442' build/debug/firmware.elf

clean:
	find build -type f -not -name .gitkeep | xargs rm -f

build/debug/firmware.elf: $(DEBUG_OBJECTS)
	$(CC) $(CFLAGS) $(DEBUG_FLAGS) -o $@ $^

build/release/firmware.elf: $(RELEASE_OBJECTS)
	$(CC) $(CFLAGS) $(RELEASE_FLAGS) -o $@ $^

build/debug/%.o: src/%.c include/*.h
	$(CC) $(CFLAGS) $(DEBUG_FLAGS) -c -o $@ $<

build/release/%.o: src/%.c include/*.h
	$(CC) $(CFLAGS) $(RELEASE_FLAGS) -c -o $@ $<
