#!/bin/bash

LOG_FILE=.bloom/bloom.log

function cleanup {
	bloom_pid="$1"
	wrapper_pid="$2"

	# wait for the wrapper to be savagely sigkilled by VS Code
	while kill -0 "$wrapper_pid"; do
		sleep 1
	done

	# cleanly exit Bloom
	kill -int "$bloom_pid"
}

# move to the workspace folder so Bloom can find the bloom.yaml file
cd "$(dirname "$0")"

# log the Bloom output
mkdir -p "$(dirname "$LOG_FILE")"
bloom > "$LOG_FILE" &

# setup the cleanup process for a clean Bloom exit
cleanup $! $$ &

# print the output of the Bloom process so VS Code can know when Bloom is ready
tail -f "$LOG_FILE"
