#!/bin/bash

DEVICE=/dev/ttyACM0

echo '##############################################'
echo '##                                          ##'
echo '##      PLEASE CYCLE THE DEVICE POWER       ##'
echo '##                                          ##'
echo '##  So unplug and plug again the USB cable  ##'
echo '##  if you are powering it via USB.         ##'
echo '##                                          ##'
echo '##############################################'

function modt {
	stat --format '%Y' "$DEVICE" 2> /dev/null || echo 0
}

t="$(modt)"
while
	sleep 1
	new_t="$(modt)"
	[ "$t" = "$new_t" ] || [ "$new_t" = 0 ]
do
	t="$new_t"
done
