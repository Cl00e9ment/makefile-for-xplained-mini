# Makefile (and VS Code config files) for XPlained Mini

![GDB UI](docs/gdb.png)
![VS Code debug UI](docs/vscode-debug.png)

## Requirements

- building
	- [Make](https://www.gnu.org/software/make/)
	- [avr-gcc](https://gcc.gnu.org/)
- uploading
	- [AVRDUDE](https://www.nongnu.org/avrdude/)
-  debugging
	- [Bloom](https://bloom.oscillate.io/)
	- [avr-gdb](https://www.sourceware.org/gdb/)
- serial console
	- [picocom](https://github.com/npat-efault/picocom)

## How to use?

- [Instructions to use this repo with Make.](./docs/howtouse-make.md)
- [Instructions to use this repo with VS Code.](./docs/howtouse-vscode.md)

## Use this repo as a boilerplate

1. Choose a license that corresponds to your needs and replace the `COPYING` file.
2. Edit this `README.md` file to your liking while keeping the [Requirements](#requirements) and [How to Use?](#how-to-use) sections.
3. Start coding in `src` and `include`.
